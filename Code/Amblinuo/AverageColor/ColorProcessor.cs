﻿using System;
using System.Drawing;

namespace Amblinuo
{
    internal static class ColorProcessor
    {

        private static readonly float vibranceFactor = 1.45f;

        public static Color IncreaseVibrance(Color color)
        {
            int r = color.R;
            int g = color.G;
            int b = color.B;

            if (color.R > color.G && color.R > color.B)
            {
                r = (int)(color.R * vibranceFactor);
                g = (int)(color.G / vibranceFactor);
                b = (int)(color.B / vibranceFactor);
                
            }

            if (color.G > color.R && color.G > color.B) {

                r = (int)(color.R / vibranceFactor);
                g = (int)(color.R * vibranceFactor);
                b = (int)(color.B / vibranceFactor);
            }

            if (color.B > color.R && color.B > color.G)
            {
                r = (int)(color.R / vibranceFactor);
                g = (int)(color.G / vibranceFactor);
                b = (int)(color.R * vibranceFactor);

            }


            r = Math.Clamp(r, 0, 255);
            g = Math.Clamp(g, 0, 255);
            b = Math.Clamp(b, 0, 255);

            return Color.FromArgb(color.A, r, g, b);

        }



    }
}
