﻿namespace Amblinuo.Config
{
    internal class AppConfig
    {

        public string AppTrayIcon { get; } = "./Res/Icon/icon.ico";
        public string AppTrayExitIcon { get; } = "./Res/Icon/exit.ico";
        
        
        public string AppTrayTitle { get; } = "Amblino";
        public string AppTrayText { get; } = "Analyse your screen and find the best matching color !";


        public string AppTrayItemAmbientLight { get; } = "Ambient Light";
        public string AppTrayItemRed { get; } = "Red";
        public string AppTrayItemGreen { get; } = "Green";
        public string AppTrayItemBlue { get; } = "Blue";
        public string AppTrayItemOn { get; } = "On";
        public string AppTrayItemOff{ get; } = "Off";
        public string AppTrayItemExit { get; } = "Exit";

        



        public int ScanStep { get; } = 15;






    }
}
