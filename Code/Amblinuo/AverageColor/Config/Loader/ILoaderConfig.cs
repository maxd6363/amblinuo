﻿namespace Amblinuo.Config.Loader
{
    internal interface ILoaderConfig
    {
        AppConfig LoadConfig();
        
        UserConfig LoadUserConfig();
    }
}
