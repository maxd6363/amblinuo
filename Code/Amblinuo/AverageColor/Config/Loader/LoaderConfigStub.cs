﻿namespace Amblinuo.Config.Loader
{
    internal class LoaderConfigStub : ILoaderConfig
    {
        public AppConfig LoadConfig()
        {
            return new AppConfig();
        }

        public UserConfig LoadUserConfig()
        {
            var userConfig = new UserConfig
            {
                AmbientMode = true
            };
            return userConfig;
        }
    }
}
