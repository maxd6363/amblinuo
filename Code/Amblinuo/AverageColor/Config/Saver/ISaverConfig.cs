﻿namespace Amblinuo.Config.Saver
{
    interface ISaverConfig
    {
        void SaveConfig(AppConfig config);
    }
}
