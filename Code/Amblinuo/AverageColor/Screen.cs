﻿using Amblinuo.Config;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Amblinuo
{
    internal class Screen
    {

        public Size Size { get; private set; }


        public int StepAverage { get => _step;

            set
            {
                if (value < 1)
                {
                    _step = 1;
                    return;
                }
                _step = value;
            }


        }

        public Bitmap Image
        {
            get
            {

                try
                {
                    Graphics.FromImage(_image).CopyFromScreen(0, 0, 0, 0, Size);
                }
                catch (System.ComponentModel.Win32Exception)
                {

                }
                return _image;
            }

        }



        private Graphics Graphics { get; }
        private int DataProcessLength {get ;}


        private readonly Bitmap _image;
        private int _step;


        public Screen(AppConfig config)
        {
            Graphics = Graphics.FromHwnd(IntPtr.Zero);
            
            Size = new Size((int)Graphics.VisibleClipBounds.Width, (int)Graphics.VisibleClipBounds.Height);

            StepAverage = config.ScanStep;

            DataProcessLength = (Size.Width * Size.Height) / (StepAverage * StepAverage);
            
            _image = new Bitmap(Size.Width, Size.Height);
        }


        public Color GetAverageColor()
        {

            int r = 0, g = 0, b = 0;
            Color currentColor;
            Bitmap currentImage = Image;

            for (int i = 0; i < Size.Width; i += StepAverage)
            {
                for (int j = 0; j < Size.Height; j += StepAverage)
                {
                    currentColor = currentImage.GetPixel(i, j);
                    r += currentColor.R;
                    g += currentColor.G;
                    b += currentColor.B;
                }
            }
            r /= DataProcessLength;
            g /= DataProcessLength;
            b /= DataProcessLength;

            r = Math.Clamp(r, 0, 255);
            g = Math.Clamp(g, 0, 255);
            b = Math.Clamp(b, 0, 255);




            return ColorProcessor.IncreaseVibrance(Color.FromArgb(255, r, g, b));
        }



    }
}
