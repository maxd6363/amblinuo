﻿using Amblinuo.Config;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace Amblinuo
{
    internal class IconCustom : Form
    {

        public event Action ExitApplicationAsked;
        public event Action<ArduinoCommand> CommandAsked;
        public event Action<Color> ColorAsked;



        private NotifyIcon NotifyIcon;
        private ContextMenuStrip ContextMenu;



        public IconCustom(AppConfig config)
        {
         
            var iconThread = new Thread((ThreadStart)

                delegate
                {


                    ContextMenu = new ContextMenuStrip();
                    


                    ContextMenu.Items.Add(config.AppTrayItemAmbientLight, null, AmbientLight_Clicked);
                    ContextMenu.Items.Add(config.AppTrayItemRed, null, Red_Clicked);
                    ContextMenu.Items.Add(config.AppTrayItemGreen, null, Green_Clicked);
                    ContextMenu.Items.Add(config.AppTrayItemBlue, null, Blue_Clicked);
                    ContextMenu.Items.Add(config.AppTrayItemOn, null, On_Clicked);
                    ContextMenu.Items.Add(config.AppTrayItemOff, null, Off_Clicked);
                    


                    ContextMenu.Items.Add(config.AppTrayItemExit, Image.FromFile(config.AppTrayExitIcon), Exit_Clicked);
                    

                    NotifyIcon = new NotifyIcon
                    {
                        Icon = new Icon(config.AppTrayIcon),
                        Visible = true,
                        BalloonTipTitle = config.AppTrayTitle,
                        BalloonTipText = config.AppTrayText,
                        Text = config.AppTrayTitle,
                        ContextMenuStrip = ContextMenu
                    };
                    Application.Run();

                });
            iconThread.Start();

        }

        private void AmbientLight_Clicked(object sender, EventArgs e)
        {
            CommandAsked.Invoke(ArduinoCommand.AMBIENT_LIGHT);
        }

        private void Blue_Clicked(object sender, EventArgs e)
        {
            ColorAsked.Invoke(Color.Blue);
        }

        private void Green_Clicked(object sender, EventArgs e)
        {
            ColorAsked.Invoke(Color.Green);
        }

        private void Red_Clicked(object sender, EventArgs e)
        {
            ColorAsked.Invoke(Color.Red);
        }

        private void Off_Clicked(object sender, EventArgs e)
        {
            CommandAsked.Invoke(ArduinoCommand.OFF);
        }

        private void On_Clicked(object sender, EventArgs e)
        {
            CommandAsked.Invoke(ArduinoCommand.ON);
        }

        private void Exit_Clicked(object sender, EventArgs e)
        {
            NotifyIcon.Visible = false;
            ExitApplicationAsked.Invoke();
        }
    }
}
