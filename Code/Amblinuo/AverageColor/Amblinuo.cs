﻿using Amblinuo.Config;
using Amblinuo.Config.Loader;
using System;
using System.Drawing;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace Amblinuo
{
    class Amblinuo : ServiceBase
    {

        private static ILoaderConfig loaderConfig;
        private static AppConfig config;
        private static UserConfig userConfig;
        private static Screen screen;
        private static IconCustom icon;
        private static ArduinoController arduinoController;
        private static Thread ambientThread;
        private static Color lastScreenColor;

        public Amblinuo()
        {
            try
            {
                Application.ApplicationExit += Application_ApplicationExit;

                loaderConfig = new LoaderConfigStub();
                config = loaderConfig.LoadConfig();
                userConfig = loaderConfig.LoadUserConfig();
                screen = new Screen(config);
                arduinoController = new ArduinoController();
                lastScreenColor = Color.Black;


                icon = new IconCustom(config);
                icon.ExitApplicationAsked += Icon_ExitApplicationAsked;
                icon.CommandAsked += Icon_CommandAsked;
                icon.ColorAsked += Icon_ColorAsked;

                StartAmbientLight();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                ExitApplicationProperly();
            }


        }

        private void Icon_ColorAsked(Color obj)
        {
            StopAmbientLight();
            arduinoController.SendCommand(obj);
        }

        private void Icon_CommandAsked(ArduinoCommand obj)
        {
            if (obj == ArduinoCommand.AMBIENT_LIGHT)
            {
                StartAmbientLight();
            }
            else
            {
                StopAmbientLight();
                arduinoController.SendCommand(obj);
            }
        }

        private void Icon_ExitApplicationAsked()
        {
            ExitApplicationProperly();
        }

        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            ExitApplicationProperly();
        }


        private void ExitApplicationProperly()
        {
            StopAmbientLight();
            arduinoController.SendCommand(ArduinoCommand.OFF);
            Application.Exit();
        }


        private void StartAmbientLight()
        {
            userConfig.AmbientMode = true;
            ambientThread = new Thread((ThreadStart)
                delegate
                {
                    while (userConfig.AmbientMode)
                    {
                        var color = screen.GetAverageColor();
                        if (color == lastScreenColor) continue;
                        lastScreenColor = color;
                        arduinoController.SendCommand(color);
                    }
                });
            ambientThread.Start();
        }


        private void StopAmbientLight()
        {
            userConfig.AmbientMode = false;
            if (ambientThread != null && ambientThread.IsAlive)
                ambientThread.Join();
            arduinoController.SendCommand(Color.Black);
        }

    }
}
