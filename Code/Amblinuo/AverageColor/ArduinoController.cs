﻿using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Management;

namespace Amblinuo
{
    internal class ArduinoController
    {

        public string PortName { get; }
        public int Baud { get; } = 9600;
        private SerialPort Port { get; }


        public ArduinoController()
        {


            using var searcher = new ManagementObjectSearcher("SELECT * FROM WIN32_SerialPort");
            string[] portnames = SerialPort.GetPortNames();
            var ports = searcher.Get().Cast<ManagementBaseObject>().ToList();
            var tList = (from n in portnames join p in ports on n equals p["DeviceID"].ToString() select n + " - " + p["Caption"]).ToList();

            foreach (string s in tList)
            {
                System.Console.WriteLine(s);
                if (s.Contains("Arduino"))
                {
                    PortName = s.Substring(0, 5).Replace(" ", string.Empty);
                    Port = new SerialPort(PortName, Baud);
                    Port.Open();
                    
                }
            }
        }

   
        public void SendCommand(ArduinoCommand command)
        {
            Port.WriteLine(command.ToString().ToUpper());
        }


        public void SendCommand(int r, int g, int b)
        {
            Port.WriteLine($"COLOR {r} {g} {b}");
        }

        public void SendCommand(Color color)
        {
            SendCommand(color.R, color.G, color.B);
        }


        ~ArduinoController()
        {
            Port.Close();
        }



    }
}
