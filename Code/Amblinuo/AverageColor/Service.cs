﻿using System.ServiceProcess;

namespace Amblinuo
{
    static class Service
    {
        static void Main()
        {
            ServiceBase[] running = new ServiceBase[]
            {
                new Amblinuo()
            };
            ServiceBase.Run(running);
        }

    }
}
