#define PIN_LED_R 10
#define PIN_LED_G 9
#define PIN_LED_B 11

/*
   Command list

   ON
   OFF
   COLOR [r] [g] [b]

*/


typedef struct {
  int R;
  int G;
  int B;
} Color;


bool equals(Color c1, Color c2) {
  return c1.R == c2.R && c1.G == c2.G && c1.B == c2.B;
}

const bool SMOOTH_COLOR = true;
const int SMOOTH_STEP = 1;
const int SMOOTH_DELAY = 3;

String command = "";
Color currentColor = {0, 0, 0};
Color targetColor = {0, 0, 0};



void setup() {

  Serial.begin(9600);
  pinMode(PIN_LED_R, OUTPUT);
  pinMode(PIN_LED_G, OUTPUT);
  pinMode(PIN_LED_B, OUTPUT);
  command.reserve(50);


}

void loop() {

  if (Serial.available() > 0) {
    command = Serial.readStringUntil('\n');
    handleCommand(command);
  }

  if (SMOOTH_COLOR) {
    setColorSoft();
  }


}


void handleCommand(String command) {



  if (command == "ON") {
    setColorHard({255, 255, 255});
    return;
  }
  if (command == "OFF") {
    setColorHard({0, 0, 0});
    return;
  }

  int r, g, b;
  if (sscanf(command.c_str(), "COLOR %d %d %d", &r, &g, &b) == 3) {
    setColorSoft({r, g, b});
  }
}



void setColorSoft(void) {
  setColorSoft(targetColor);
}


void setColorSoft(Color c) {
  targetColor = c;

  if (equals(c, currentColor)) return;

  if (targetColor.R > currentColor.R) {
    currentColor.R += SMOOTH_STEP;
    if (currentColor.R > 255) currentColor.R = 255;
  }
  else {
    currentColor.R -= SMOOTH_STEP;
    if (currentColor.R < 0) currentColor.R = 0;
  }

  if (targetColor.G > currentColor.G) {
    currentColor.G += SMOOTH_STEP;
    if (currentColor.G > 255) currentColor.G = 255;
  }
  else {
    currentColor.G -= SMOOTH_STEP;
    if (currentColor.G < 0) currentColor.G = 0;
  }

  if (targetColor.B > currentColor.B) {
    currentColor.B += SMOOTH_STEP;
    if (currentColor.B > 255) currentColor.B = 255;
  }
  else {
    currentColor.B -= SMOOTH_STEP;
    if (currentColor.B < 0) currentColor.B = 0;
  }

  setColorHard(currentColor);
  delay(SMOOTH_DELAY);  
}


void setColorHard(Color c) {
  analogWrite(PIN_LED_R, c.R);
  analogWrite(PIN_LED_G, c.G);
  analogWrite(PIN_LED_B, c.B);
}
