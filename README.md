# Amblinuo

## What is it ?

Amblinuo is an ambient light display for Windows, working with an Arduino. It is like the **Ambilight** from Philips.

## How does it work ?

You can see the execution in three steps : 

### Step 1 : The image is rendered to the display

Windows and your graphics card are doing this step ! 

<img src="https://i.ibb.co/KrDN7hK/Amblinuo-Software-1.png" alt="Amblinuo Step 1 : The image is render to the display" >

### Step 2 : The image is sampled by Amblinuo

Amblinuo takes a grid of dots and process the average color of the display. Very simple and you can space out the dots if you have a high resolution, like 1440p or 4k. 

<img src="https://i.ibb.co/j5RdKdB/Amblinuo-Software-2.png" alt="Amblinuo Step 2 : The image is sampled by Amblinuo" >


### Step 3 : The color is sent to the Arduino

Amblinuo use the serial connection between Windows and the Arduino with simple commands : 

```
ON
OFF
COLOR 156 23 036

```
The Arduino is also programmed to handle those commands, and tell the LED strip to change color.

<img src="https://i.ibb.co/WyR3Tcf/Amblinuo-Hardware.png" alt="Amblinuo Step 2 : The image is sampled by Amblinuo" >


### Step 4 : Congratulation you have a cheap Ambilight, DIY and fully customizable !


<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="https://i.ibb.co/V3Rd0nk/image-2021-01-19-152712.png">
      <source src="Documentation/Amblinuo - Proof of concept.mp4" type="video/mp4">
  </video>

</figure>

![Amblinuo](Documentation/Amblinuo - Proof of concept.mp4)

[**Youtube link**](www.youtube.com/watch?v=YUuGQn7Mv2k)



## Software

<img src="https://i.ibb.co/bQTTNBJ/image-2021-01-19-155429.png" alt="Amblinuo Software" >

Amblinuo is an App Tray application, no messy window, it is simple and efficient !


## License

MIT License
Copyright (c) 2020 Amblinuo
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
